# Social Media

## History Index

- sm_enforcement_history-2019 on ES6 in QA, currently on ES2 in production

## Posts Index

- social-media-posts-2021-id-fix (ES6)
- social-media-pages-2021-id-fix (ES6) - not needed

## Update Index

Same process as described for Websites.

## Test Setup

### Find Data

```
curl --location --request GET 'https://vpc-bp-qa-elasticsearch-6-a2dkwzrmhpswlrodvp6ph7v6ke.us-west-2.es.amazonaws.com/social-media-posts-2021-id-fix/_search' \
--header 'Content-Type: application/json' \
--data-raw '{
    "size": 1,
    "from": 0,
    "_source": ["accountID", "enfFirstEnforcedDate", "enfLastEnforcedDate", "enfEnforcementCount", "globalItemId", "enfEnforceStatus"],
    "query": {
        "bool": {
            "must": [
                {
                    "term": {
                        "enfEnforceStatus.raw": "SENT"
                    }
                },
                {
                    "range": {
                        "enfLastEnforcedDate": {
                            "time_zone": "America/Los_Angeles",
                            "gte": "2021-08-01",
                            "lte": "2021-09-01",
                            "include_lower": true,
                            "include_upper": "false"
                        }
                    }
                }
                
            ]
        }
    },
    "sort" : [
      {"enfFirstEnforcedDate" : {"order" : "asc"}}
   ]
}'

```

### Single Document

```
curl --location --request GET 'https://vpc-bp-qa-elasticsearch-6-a2dkwzrmhpswlrodvp6ph7v6ke.us-west-2.es.amazonaws.com/social-media-posts-2021-id-fix/social-media-posts/14731444_cca81f1b497e387eadf6db9d6b4c7b4f'

```

### Remove Field

``` 
curl --location --request POST 'https://vpc-bp-qa-elasticsearch-6-a2dkwzrmhpswlrodvp6ph7v6ke.us-west-2.es.amazonaws.com/social-media-posts-2021-id-fix/social-media-posts/14731444_cca81f1b497e387eadf6db9d6b4c7b4f/_update' \
--header 'Content-Type: application/json' \
--data-raw '{
  "script": {
    "source": "ctx._source.remove(\"enfFirstEnforcedDate\")"
  }
}
'

```


