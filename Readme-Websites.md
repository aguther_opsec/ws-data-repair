# Data Repair for Missing First Enforcement Date in Websites

A current bug produces main grid entries in Websites that have the enforcement status SENT and an enforcement entry in the subgrid, but are missing the first enforcement date.
Those documents need to be fixed.
The problem was introduced beginning of August and only affect enforcement requests that were process after that time.
Document enforced during this month that are in status SENT but are missing the first enforcement date need to be fixed before the end of the month. 
The following describes the data repair process.

## Processing Rules

### Find Documents With Missing First Enforcement Date

Locate all documents that have a missing first enforcement date. The field has to be missing. It cannot be set to null.

### Find Enforcement History

Based on the global item ID and the account ID find all corresponding documents in the enforcement history index. Order 
by first enforcement date in ascending order and take the oldest one, which is the first one in the list. 
 

### Update Document With Extracted Date

The date found in the previous step will be used as the date for adding the first enforcement date to the records that need to be updated.

## Project Setup

The following dependencies need to be installed:

```shell
npm install node-fetch
npm install luxon
npm install log4js
npm init -y  
```

