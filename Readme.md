# Data Repair for Missing First Enforcement Date in WS and SM

- [Social Media](Readme-Social-Media.md)
- [Websites](Readme-Websites.md)

## Data Repair

See [Data Repair Run](docs/Data-Repair-Run.md)

## Server

### PROD

* ES2 - https://elastic.bp.opsecol.net
* ES6 - vpc-bp-prod-elasticsearch-7adkqa5l2zdc5zqbgirrsj7r5i.us-west-2.es.amazonaws.com

### QA

* ES2 - http://elastic.qa.bp.opsecol.net
* ES6 - https://vpc-bp-qa-elasticsearch-6-a2dkwzrmhpswlrodvp6ph7v6ke.us-west-2.es.amazonaws.com

## Indices

See also the document with [list of indices](docs/Indices.md).

### PROD

#### Websites

* URLs -
* History -

#### Social Media

* Posts - 
* History -

### QA

#### Websites

* URLs -
* History -

#### Social Media

* Posts -
* History -

