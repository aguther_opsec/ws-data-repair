# Indices

## Elasticsearch 6

### QA  (ES-6.8)
``` 
health status index                          uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   action-history-2020            iYlxY2YsQKuCp1-5B9mc9Q   5   0    4122744           10        490            490
green  open   action-history-2020-1          4Mo7vitWTu-jO-lra-LgRQ   5   0   10634394            1       1722           1722
green  open   action-history-2021-1          pcvfZIc7RGuEc0lrPhn_qA   5   0          0            0          0              0
green  open   action-history-obp-2020        NnUlv1LlSwq2BI1GyxGG-w   5   0   10661437          491       1232           1232
green  open   action-history-test            XRVmla6cQHGbUA0dmSe9rQ   5   0          1            0          0              0
green  open   document_library               grcnNM4cSCuO96uNFCrv-w   5   1       1325           11          1              0
green  open   domain_contacts                57SLhT5tSu6bloR3kvshZQ   5   0      12801           16         37             37
green  open   domain_infos                   brXybJxFRWm9dNePQo6Zww   5   1    6336584       133651       4464           2235
green  open   domain_names_20210822          YPAoMVO2R_yVUw0Xd6_OvA   5   1  319710736            0      77542          38894
green  open   domain_names_20210823          xwBNJGsgRgGqr1X-4PkZSQ   5   1  319690696            0      77792          38923
green  open   domain_names_20210824          TUH1y3hyRf2AwD0PxPoilw   5   1  319735518            0      77794          38779
green  open   domain_names_20210825          -_ZtdbGaRii0RyL8TTxR3Q   5   1  319788209            0      77692          38883
green  open   enforcement-history            pzLpKx-sSNGoNMlsibIlKw   5   1      67202           14        267            133
green  open   enforcement-mailbox            ifPm1NJKTYeIRFg3PaDsOw   5   1     228231          492       1174            587
green  open   enforcement-pagecache-files    aZZaNgmyRLWMjZ-E2yFVpw   5   0     131998            0         82             82
green  open   field_mapping                  l6SZPClmS3653_k7YZPKrQ   5   0        109            5          0              0
green  open   file_management                zqnNoFp1R_yJMOWyG9DPyA   5   0      24930            5          8              8
green  open   image-recognition              3lVcU-2sSbq7X-HYRiNv_A   5   1    1253041            0        181             90
green  open   image-recognition-poc          qvCpxFM_TqiajGj54If69g   5   1         10            0          0              0
green  open   images_2021                    T6TUyg5_TTyE5LT2CdQOEg   5   1    3095476        18605       3745           1872
green  open   mm-proxy-turnover              hcfclhs_RpOTlljbaoC32Q   5   1          2            0          0              0
green  open   mp_compliance                  zctaWsFMSS6HNf70SVarKA   5   0     142337         1209        480            480
green  open   mp_enforcement_history         JIdeFZQnQainrYP7s2wIYQ   5   0     154471            0        554            554
green  open   mp_enforcement_history_test    yPh80iwfQUubcr3pm3Myow   5   0    1598445            0       5053           5053
green  open   mp_seller_info_request         0_ROcLqkSJ2AFH7rwEyTaA   5   1     528896         2150       1243            621
green  open   mp_seller_information_history  Q_V6YisqRpmKZFcdqzez5A   5   0     224181        59193        102            102
green  open   scans_2018                     khVyPGvARJKOtAu65KfvCQ   5   1   10035546       164673     135768          67919
green  open   seller_item_aggregate          unPGLwxqTGKS6ybSIgsp9g   5   1     902008       284158       1329            666
green  open   sm-enforcement-history-detail  tqq9oLL3RKmiBpW2W1jr3Q   5   0      17808           20         65             65
green  open   sm_enforcement_history-2019    6CtGcaFbSQy1rHOZgYvQ7Q   5   0     312378           26        122            122
green  open   social-media-pages             lZvteyFjS1uypsqQv5zJrQ   5   1          3            0          0              0
green  open   social-media-pages-2020        PgSZ1yojTruM5tDEwG81oA   5   1     431595        87627        535            265
green  open   social-media-pages-2021        9bM3BW9rSTGVdPdrUt3RQA   5   1      31023           29         46             23
green  open   social-media-pages-2021-id-fix -fQJlgT9RJqLFGnbtgK8Sw   5   0      35266         3960         30             30
green  open   social-media-posts             MGlHvAL5TvWncxBKJ2dYwA   5   1          6            0          0              0
green  open   social-media-posts-2020        k3D0Y4ClSqWOIwrI4Hl_2Q   5   1    1548184        16285      23776          11894
green  open   social-media-posts-2021        TAihB8ZmQZe3n9pCV21QwQ   5   1      69968          562        428            214
green  open   social-media-posts-2021-id-fix VZcvkYPkQS-26hrj_Hjr4w   5   0      82172        13834        452            452
green  open   url                            aWIggjcKTe24lmDC2Bqy3w   5   1          6            0          0              0
green  open   websites_domains-2021          qhJLyWinSwS-zF5WXEyp8w   5   0    7309245        24877       3904           3904
green  open   websites_tagged                mnPuLAXjTtSuOafScej2Pg   5   1      20984         2505         49             24
green  open   websites_tagged_reindexed      O-4plh9VSTO95reopXyEtg   5   1       6497            0         13              6
green  open   websites_urls-2021             aEuaiFzGTtOZuzZx0hb7iQ   5   1    7802110       335710      17399           8675
green  open   ws_enforcement_history         v85enw3oTNefKGf6_Ccj9Q   5   1        962            0          1              0
green  open   ws_enforcement_history-2020    9_a9LhOSQmeywSdB09D7Dw   5   1     210948           21        221            110

```

### Stage  (ES-6.8)
```
health status index                                    uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   action-history-2018.12-1                 PfGO3zSRTlKv4AVzysmQPQ   5   0      98826            0         16             16
green  open   action-history-2019                      WLjcqZ8rRQSLgWhyaFCSig   5   0    5028590            6        746            746
green  open   action-history-2019.03-1                 EqqbcWKrTZaqAv6P9RTc5g   5   0         48            0          0              0
green  open   action-history-2020                      rRnfExrCRkSMwJAqtw0SoA   5   0   13152509           95       2069           2069
green  open   action-history-2020-1                    32V6ZgZzTcOWZ8PhpUBFJQ   5   0     982252        12233        137            137
green  open   action-history-obp-2020                  kSoiKv9CSMuzgNjSkbnMKQ   5   0    4150153        43176        451            451
green  open   action-history-test                      MS06oJyMTyy-1xJK1EKVuw   5   0          1            0          0              0
green  open   backup-action-history-2020               nc1KFyniSKuev8Sj0QV0kA   5   1   13152604      2497604       6178           3089
green  open   document_library                         mEC1p8rCQEmjH1oDotPfPA   5   0        476            5          0              0
green  open   domain_contacts                          SNb1IjOVTRus3WGVqL7VaQ   5   0      27363         2222         63             63
green  open   domain_infos                             vnpuxg1STwSE9UsT8CkxcQ   5   0    4586117        36931       1487           1487
green  open   domain_names_20210823                    BW4bxzCNQG-oYpNBDXneOQ   5   1  296291601            0      72441          36173
green  open   domain_names_20210825                    7lNxInQ7SMerQgoJjAYXIA   5   1  319787770            0      77921          38905
green  open   enforcement-history                      AUuP9PbrShukWru64xnEAA   5   1      26932          382        122             61
green  open   enforcement-mailbox                      2zhVKJkKQZKv5yR12fXglA   5   1      44176            0         96             48
green  open   enforcement-pagecache-files              wUfTpYuzS5mS_M0XSYPzgw   5   0      23791            0         17             17
green  open   file_management                          0s150uOFQZ2ibf3qtJm47g   5   0     519670            4        164            164
green  open   image-recognition                        vXcjBXjtQX-PI2-sTvdSPA   5   1     120772            0         22             11
green  open   images                                   T3pBfCH3T6CoAKWbsvFNQg   5   1     617341         2608        764            382
green  open   mp_compliance                            MOT6ir2tRmGMxy8hb_630w   5   0       9539          997         35             35
green  open   mp_seller_info_request                   l6EIqvuDT168qjdhl_AsyQ   5   1      15448           30         84             41
green  open   mp_seller_information_history            -PaeLKWPRp-vxiCL2r6lKg   5   0     160975        25506         56             56
green  open   reindexed_items_detections-2021-1        92k9HJc8QSuSLJ3Ny6HnRA   5   1      74346            0         73             36
green  open   scans_2018                               PF5QZs1gTDeVjb8sO14jCQ   5   1    4202121        45631      60033          30016
green  open   seller_item_aggregate                    x07ZOtkXSTKr3yWTAdsgHw   5   1      32080          841         44             22
green  open   sm-enforcement-history-detail            g4o--xdNQgC3kNkwTb7c9Q   5   0      11850           24         19             19
green  open   sm_enforcement_history-2019              8ppoFx4OQoGH0litkshmUw   5   0      44097            0         17             17
green  open   social-media-pages-2020                  S6PkVvLwRH-QCCbhrYVOmQ   5   1     348754         4363        397            199
green  open   social-media-posts-2020                  Zg2FejBuRMuJfSCLi_oqKw   5   1     448778        19894       2212           1106
green  open   test-action-history-2020                 q6_SiGI0QcaiwHuuWpMHIw   5   1   13152604            0       5087           2543
green  open   websites_domains-2019-1                  -dNmSn_6RnW8AXiHpqgjVA   5   0    7136998       128696       3713           3713
green  open   websites_tagged                          W5iQNmm7QMa2jqEvDmNJuA   5   1      40607          814         91             45
green  open   websites_tagged_exclusion                JLcE4aTmRgW-AaeZrwCWDA   5   0     531356            0        270            270
green  open   websites_tagged_reindexed                EXxB17rXTXmAL9scRqAyrg   5   0      22266            0         26             26
green  open   websites_urls-2019-1                     brG0c66oTrmMkkgHIn1Qfw   5   1    9453413       207592      21240          10620
green  open   websites_urls-2019-1-compliance-meta-fix VW_co3nhTpGJVu97r6cxqQ    5   1    9453412      2254238      26195          13097
green  open   websites_urls-2021                       iSU-Q1U_T1e7qWj7vUUXFw   5   1    9484017       330966      22209          11098
green  open   ws_enforcement_history-2020              EM2upntOT2-2ngTOlxKAIA   5   0      87499           12         49             49
```

### Prod  (ES-6.8)

``` 
health status index                         uuid                   pri rep docs.count docs.deleted store.size pri.store.size
green  open   action-history-2018.12-1      l4ynQHdLTRSbfMq_ZWjeFQ   5   1     173222            0         61             30
green  open   action-history-2019           XBJc1Jo3RGOcBZ7xSYa9Vw   5   1   49766904        50841      13677           6838
green  open   action-history-2019.02-1      MMLy5X1nSzS31vXQr81y9w   5   1    1135654            0        371            185
green  open   action-history-2020           kNvMtsU3TDCy_g081Tuefw   5   1  133110441      3760292      41724          20862
green  open   action-history-2020-1         H2aZin6CQO6GK1tk0enICw   5   1  108470942      2733951      35388          17550
green  open   action-history-2021-1         M_Sbyc5PTkeujieCLrFyyw   5   1          0            0          0              0
green  open   action-history-obp-2020       Af_SwwfYSkSg8m4v7orRSw   5   1  139925017     53410762      44454          22185
green  open   document_library              r9vFpCNTTiGG6zxCtLEu5Q   5   1       1123           14          2              1
green  open   domain_contacts               k67LLeecTqWineBvs0kN5w   5   1     185623        14480        925            462
green  open   domain_infos                  tcwcVtBwQTeKX0guugtPtg   5   1    7414602      2326211      10580           4398
green  open   domain_names_20210822         VcyIYkliSnCjf0C3oCTg4A   5   1  319710736            0      78786          39417
green  open   domain_names_20210823         IHP5t2HTSR-vpIQSXVZaXA   5   1  319690950            0      78603          39347
green  open   domain_names_20210824         YsNP3BjORBOBxzvuKJCygg   5   1  319735518            0      78457          39171
green  open   domain_names_20210825         tgXZmgVEQjONTECX9VfKfQ   5   1  319788209            0      79120          39560
green  open   enforcement-history           UujrqNS5RsmzYf6LPXX4aA   5   1     307535          229       2935           1454
green  open   enforcement-mailbox           4ZB-cKiaQwe7pOgokoW1Iw   5   1    1108269            0       5504           2748
green  open   enforcement-pagecache-files   VgriSnrERMSzzCginLcGvw   5   1    2843526            0       4917           2456
green  open   file_management               0YXuJxw3S5WdgPK3grcltw   5   1    2738628            0       1818            909
green  open   image-recognition             st501h0ATfeFiONd3ZTaQw   5   1    4875809          695        742            371
green  open   mp_compliance                 n_VijLg3R2eMyXHMcH2lSQ   5   1    5513454       116429      32102          16037
green  open   mp_seller_info_request        Xm1JyTdpR4Otm8tbsHtTlg   5   1     180765         2346       1347            673
green  open   mp_seller_information_history emejqcXSTY-bry81ksYE7g   5   1   18822813            2      11135           5567
green  open   scans_2018                    rvFZS6ERTnKosT3m_Iw3mw   5   1  209569043     56786343    2933382        1459419
green  open   seller_item_aggregate         CYI_tiTuQCyxX0UCr0zttg   5   1   39578143     15661849      64852          32603
green  open   social-media-pages-2019       silyi7OoTQSKlwXWCMicBg   5   1    1416725            0       1829            914
green  open   social-media-pages-2020       vFeygBNETcaEb12sOLuZlQ   5   1    3858641       417368       5811           2913
green  open   social-media-posts-2019       go5cxNVLRMGncfeIey-gEw   5   1    1480538            0       4131           2065
green  open   social-media-posts-2020       AZZDQSsdTCimg9uH6FxfnA   5   1    7258228       276596      55566          27754
green  open   websites_domains-2019-1       W8-zncEfT3G7eSBkkbh8sA   5   1   10194407      3352955      22280          11352
green  open   websites_tagged               8F6UKyJLR6WxmC_onaandw   5   1   11597425      3632914      38524          19052
green  open   websites_urls-2021            hpM31RhKSbWrfeBD2vk7CQ   5   1   52546135      7440472     233526         116478
green  open   ws_enforcement_history        ptLfO9LPRPCNBzcUWFuU-g   5   0     159932            0         70             70
green  open   ws_enforcement_history-2020   nMft3-iKTWeIXQeTDYE6wg   5   0    6126482          864       2938           2938

```

## Elasticsearch 2

### QA (ES-2)
``` 
health status index                                      pri rep docs.count docs.deleted store.size pri.store.size 
green  open   document_library                             5   1        432            4    895.3kb        447.6kb 
green  open   document_library_temp                        5   1        556            0      1.1mb        594.5kb 
green  open   domain_infos                                 5   1    2319029       521416      2.3gb          1.1gb 
green  open   domain_names_20210824                        5   1  319735518            0     70.3gb         35.1gb 
green  open   domain_names_20210825                        5   1  319788209            0     73.7gb         38.5gb 
green  open   enforcement-history                          5   1      65385         4451    311.6mb        155.8mb 
green  open   enforcement-mailbox                          5   1     224424          127      1.1gb        598.3mb 
green  open   items_detections                             5   1     275237            0     89.2mb         44.5mb 
green  open   legacy_case_data                             5   1     319347            0      1.3gb          709mb 
green  open   mm-proxy-distribution-stats                  5   1     437254            0    117.9mb         58.9mb 
green  open   mm-proxy-turnover                            5   1       8590            0      1.4mb          749kb 
green  open   mp_compliance                                5   1      38883         1176    464.9mb        232.4mb 
green  open   mp_enforcement_history                       5   1     280981        87094      3.1gb          1.5gb 
green  open   mp_enforcement_history_bkup3sep20            5   1      88026            0    787.2mb        393.6mb 
green  open   mp_enforcement_history_bng_13200             5   1    9410000         2690     59.2gb         29.6gb 
green  open   mp_enforcement_history_search                5   1          1            0      7.5kb          3.7kb 
green  open   mp_seller_info_request                       5   1     191553            4    585.7mb        292.8mb 
green  open   mp_seller_information_history                5   1      56572            0     32.1mb           16mb 
green  open   nested_demo                                  5   1          9            0       35kb         17.5kb 
green  open   proxy_error_stats                            5   1     502275            0    116.4mb         58.2mb 
green  open   scad_test                                    5   1        100            0      7.9mb          3.9mb 
green  open   seller-item-aggregate                        5   1          1            0     31.5kb         15.7kb 
green  open   seller_item_aggregate_temp                   5   1     605746            0    771.2mb        385.6mb 
green  open   sm-enforcement-history-detail                5   1      18493          107    279.3mb        139.6mb 
green  open   sm_enforcement_history                       5   1       6342            6     10.9mb          5.4mb 
green  open   sm_enforcement_history-2019                  5   1     311745         3586    304.1mb          152mb 
green  open   websites_domains                             5   1       1816            0      4.2mb          2.1mb 
green  open   ws_enforcement_history-2019                  5   1     210069        24750    282.9mb        141.4mb 
green  open   ws_enforcement_history-2020                  5   1         89            0      365kb        182.5kb 

```

### Stage (ES-2)
``` 
health status index                               pri rep docs.count docs.deleted store.size pri.store.size 
green  open   aliases                               5   1          0            0      1.5kb           795b 
green  open   domain_names_20210824                 5   1  319735518            0     70.4gb         35.2gb 
green  open   domain_names_20210825                 5   1  319788209            0     70.5gb         35.2gb 
green  open   enforcement-history                   5   1      25347            6    117.9mb         58.9mb 
green  open   enforcement-mailbox                   5   1      42408            0    249.4mb        124.7mb 
green  open   hvt_edges_aug12-2019                  5   1      66546          598     26.6mb         13.3mb 
green  open   hvt_summary_aug12-2019                5   1      15744          102       14mb            7mb 
green  open   hvt_vertices_aug12-2019               5   1      64341         2355    106.1mb           53mb 
green  open   legacy_case_data                      5   1     319347            0      1.3gb          707mb 
green  open   mm-proxy-turnover                     5   1        469            0    359.1kb        179.5kb 
green  open   mp_enforcement_history                5   1      27750         7368    346.1mb        196.1mb 
green  open   performance_metrics_index             5   1         20            0    143.6kb         71.8kb 
green  open   performance_submitted_request_index   5   1        423            1    283.1kb        141.5kb 
green  open   sm-enforcement-history-detail         5   1      19631           19    147.9mb         75.5mb 
green  open   sm_enforcement_history                5   1       1798            7      3.8mb          1.9mb 
green  open   sm_enforcement_history-2019           5   1      44097           19     52.6mb         26.3mb 
green  open   ws_enforcement_history-2019           5   1      84266            4    102.5mb         51.2mb 

```

### Prod (ES-2)
``` 
health status index                                               pri rep docs.count docs.deleted store.size pri.store.size 
green  open   .kibana                                               1   1         43            1    294.1kb          147kb 
green  open   .kibana-backup                                        5   1         27            0    158.8kb         79.4kb 
green  open   account_detection_count_marketplaces_index_14728465   2   1   41757293            0      4.4gb          2.2gb 
green  open   af_ews_org_config                                     5   1       3167            6      1.5mb          795kb 
green  open   af_ews_org_config_bkup                                5   1       2689            0      1.1mb        608.3kb 
green  open   af_ews_org_config_demo                                1   1          7            4     50.5kb         25.2kb 
green  open   enforcement-mailbox                                   5   1    1034354            0      9.8gb          4.9gb 
green  open   field_mapping                                         5   1        214            0    368.9kb        184.4kb 
green  open   legacy_case_data                                      5   1    6898639       360247      140gb           70gb 
green  open   legacy_case_data_lbrands                              5   1    1743133       175139     48.6gb         24.7gb 
green  open   mm-proxy-distribution-stats                           5   1  163176821            0     33.6gb         16.8gb 
green  open   mm-proxy-turnover                                     5   1        361            0    324.7kb        162.3kb 
green  open   mp_enforcement_history                                5   1   14108391       378881     95.1gb         47.5gb 
green  open   proxy_error_stats                                     5   1        520            0      424kb          212kb 
green  open   scans_2018                                            5   1  210374620    116846946      2.8tb          1.4tb 
green  open   search                                                5   1          2            0     12.2kb          6.1kb 
green  open   sm-enforcement-history-detail                         5   1     274012         1350      1.2gb        663.4mb 
green  open   sm_enforcement_history                                5   1      28819           17       56mb           28mb 
green  open   sm_enforcement_history-2019                           5   1    1140954       247808      1.3gb        674.5mb 
green  open   websites_domains-2019-1                               5   1    8488101      4598246     26.9gb         13.6gb 
green  open   websites_urls                                         5   1          0            0      1.5kb           795b 
green  open   websites_urls-2019-1                                  5   1   46794699     18440867    274.6gb        137.3gb 
green  open   ws_enforcement_history                                5   1      39291           13    108.9mb         54.4mb 

```
