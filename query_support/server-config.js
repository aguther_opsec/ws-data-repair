exports.SERVER_QA_ES_6 = "https://vpc-bp-qa-elasticsearch-6-a2dkwzrmhpswlrodvp6ph7v6ke.us-west-2.es.amazonaws.com";
exports.SERVER_PROD_ES_2 = "https://elastic.bp.opsecol.net";
exports.SERVER_PROD_ES_6 = "https://vpc-bp-prod-elasticsearch-7adkqa5l2zdc5zqbgirrsj7r5i.us-west-2.es.amazonaws.com";
exports.SERVER_STAGE_ES_2 = "https://elastic.stg.bp.opsecol.net";
exports.SERVER_STAGE_ES_6 = "https://vpc-bp-stage-elasticsearch-6-7tejc37qy443rxcpysktltko3q.us-west-2.es.amazonaws.com";