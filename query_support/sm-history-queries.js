"use strict";

const fetch = require("node-fetch");
const servers = require("./server-config");

const log4js = require("log4js");
log4js.configure("config/log4js.json");
const log = log4js.getLogger("sm-history-queries");

global.fetch = fetch;
global.Headers = fetch.Headers;

const myHeaders = new global.Headers();
myHeaders.append("Content-Type", "application/json");

exports.indexName = new Map([
    ["qa", "sm_enforcement_history-2019"],
    ["stage", "sm_enforcement_history-2019"],
    ["prod", "sm_enforcement_history-2019"],
]);

exports.server = new Map([
  ["qa", servers.SERVER_QA_ES_6],
  ["stage", servers.SERVER_STAGE_ES_6],
  ["prod", servers.SERVER_PROD_ES_2]
]);

function getHistoryQuery(accountId, itemId) {
  log.debug(`Building get history query for accountId ${accountId} and itemId ${itemId}`);
  return JSON.stringify({
    "query": {
      "bool": {
        "must": [
          {
            "term": {
              "accountId": accountId
            }
          },
          {
            "term": {
              "pageEnforcementStatus": "SENT"
            }
          },
          {
            "nested": {
              "path": "posts",
              "query": {
                "bool": {
                  "must": [
                    {
                      "term": {
                        "posts.itemId": itemId
                      }
                    }
                  ]
                }
              }
            }
          }
        ]
      }
    },
    "sort": [
      {
        "dateOfEnforcement": {
          "order": "asc"
        }
      }
    ]
  });
}

exports.buildRequestOptions = function(accountId, itemId) {
  return {
    method: "POST",
    headers: myHeaders,
    body: getHistoryQuery(accountId, itemId),
    redirect: "follow"
  };
};