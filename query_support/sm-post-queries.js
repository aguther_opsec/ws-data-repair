const servers = require("./server-config");
const fetch = require("node-fetch");

const log4js = require("log4js");
log4js.configure("config/log4js.json");
const log = log4js.getLogger("sm-posts-queries");

global.fetch = fetch;
global.Headers = fetch.Headers;

const myHeaders = new global.Headers();
myHeaders.append("Content-Type", "application/json");

exports.type = "social-media-posts";

// All indices are on ES-6 for posts

exports.indexName = new Map([
    ["qa", "social-media-posts-2020"],
    ["stage", "social-media-posts-2020"],
    ["prod", "social-media-posts-2020"]
]);
exports.server = new Map([
    ["qa", servers.SERVER_QA_ES_6],
    ["stage", servers.SERVER_STAGE_ES_6],
    ["prod", servers.SERVER_PROD_ES_6]
]);

function getQuery (size = 1, from = 0, startDate = "2021-08-01", endDate = "2021-09-01") {
    log.debug(`start date: ${startDate} - end date: ${endDate}; from: ${from}, size: ${size}`);
    const raw = JSON.stringify({
        "size": size,
        "from": from,
        "_source": [
            "accountID",
            "enfFirstEnforcedDate",
            "enfLastEnforcedDate",
            "enfEnforcementCount",
            "globalItemId",
            "enfEnforceStatus"
        ],
        "query": {
            "bool": {
                "must": [
                    {
                        "term": {
                            "enfEnforceStatus.raw": "SENT"
                        }
                    },
                    {
                        "range": {
                            "enfLastEnforcedDate": {
                                "time_zone": "America/Los_Angeles",
                                "gte": startDate,
                                "lte": endDate,
                                "include_lower": true,
                                "include_upper": false
                            }
                        }
                    }
                ],
                "must_not": [
                    {
                        "exists": {
                            "field": "enfFirstEnforcedDate"
                        }
                    }
                ]
            }
        },
        "sort": [
            {
                "enfLastEnforcedDate": {
                    "order": "asc"
                }
            }
        ]
    });
    log.debug("Query to find update candidates", raw);
    return raw;
}

function updateDocument(enfFirstEnforcedDate) {
    return JSON.stringify({
        "doc": {
            "enfFirstEnforcedDate": enfFirstEnforcedDate
        }
    });
}

exports.buildRequestOptions = function (size = 1, from = 0, startDate = "2021-08-01", endDate = "2021-09-01") {
    return {
        method: "POST",
        headers: myHeaders,
        body: getQuery(size, from, startDate, endDate),
        redirect: "follow"
    };
};

exports.buildUpdateOptions = function (enfFirstEnforcedDate) {
    return {
        method: "POST",
        headers: myHeaders,
        body: updateDocument(enfFirstEnforcedDate),
        redirect: "follow"
    };
};