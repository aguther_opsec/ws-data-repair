"use strict";

const fetch = require("node-fetch");

const log4js = require("log4js");
const servers = require("./server-config");
log4js.configure("config/log4js.json");
const log = log4js.getLogger("ws-history-queries");

global.fetch = fetch;
global.Headers = fetch.Headers;

const myHeaders = new global.Headers();
myHeaders.append("Content-Type", "application/json");

exports.indexName = new Map([
  ["qa", "ws_enforcement_history-2020"],
  ["stage", "ws_enforcement_history-2020"],
  ["prod", "ws_enforcement_history-2020"]
]);
exports.server = new Map([
  ["qa", servers.SERVER_QA_ES_6],
  ["stage", servers.SERVER_STAGE_ES_6],
  ["prod", servers.SERVER_PROD_ES_6]
]);

function getHistoryQuery(accountId, itemId) {
  log.debug(`Building query to get WS history for accountId ${accountId} and itemId ${itemId}`);
  return JSON.stringify({
    "query": {
      "bool": {
        "must": [
          {
            "term": {
              "accountId": accountId
            }
          },
          {
            "term": {
              "domainEnforcementStatus": "SENT"
            }
          },
          {
            "nested": {
              "path": "urls",
              "query": {
                "bool": {
                  "must": [
                    {
                      "term": {
                        "urls.itemId": itemId
                      }
                    }
                  ]
                }
              }
            }
          }
        ]
      }
    },
    "sort": [
      {
        "dateOfEnforcement": {
          "order": "asc"
        }
      }
    ]
  });
}

exports.buildRequestOptions = function(accountId, itemId) {
  return {
    method: "POST",
    headers: myHeaders,
    body: getHistoryQuery(accountId, itemId),
    redirect: "follow"
  };
};