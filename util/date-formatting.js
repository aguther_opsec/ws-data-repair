const {DateTime} = require("luxon");
// const defaultDateFormat = "ccc, LLL dd, yyyy, HH:mm:ss.SSS ZZZZ";
const defaultDateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'";

// "2021-08-16T20:49:45Z"

const log4js = require("log4js");
log4js.configure("config/log4js.json");
const log = log4js.getLogger("fetch-history");

exports.formatEpochDate = function formatEpochDate (d, format = defaultDateFormat) {
    if (d) {
        try{
            const dt = DateTime.fromMillis(d, { zone: "UTC" });
            return dt.toFormat(format);
        } catch (error){
            log.error(`Date is not expected Epoch Date: "${d}". Trying now ISO format...`);
            return DateTime.fromISO(d).toFormat(defaultDateFormat)
        }


        // return dt.toLocaleString(DateTime.DATETIME_FULL_WITH_SECONDS);

    } else {
        return "N/A";
    }
}