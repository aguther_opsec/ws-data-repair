/* jshint esversion: 6 */
"use strict";

// const fetch = require("sync-fetch");
const fetch = require("node-fetch");
// const { DateTime } = require("luxon");
const enforcementHistory = require("./query_support/ws-history-queries");
const urls = require("./query_support/ws-urls-queries");
const dateFormat = require("./util/date-formatting");

const log4js = require("log4js");
log4js.configure("config/log4js.json");
const log = log4js.getLogger("ws-data-repair");


/**
 * Extracts the date from the enforcement history document
 * @param result result set from the query
 * @param accountId account id for logging
 * @param itemId item id for logging
 * @returns map with update details
 */
function extractDateFrom (result, accountId, itemId) {

    const rh = result.hits;
    // console.debug(rh)
    if (rh.total > 0) {
        log.debug("extracting date from given result...");
        const hits = rh.hits;
        log.debug("First element of hits:", JSON.stringify(hits[0], undefined, 2));
        const source = hits[0]._source;
        const documentId = hits[0]._id;
        // log.debug("Source:", JSON.stringify(source, undefined, 2))
        const d = source.dateOfEnforcement;
        // log.debug(`Extracted date: ${d}`)
        log.info(`Formatted date of enforcement: ${dateFormat.formatEpochDate(d)}; Date as found in document: ${d}`);
        // return d
        const updateDetails = new Map();
        updateDetails.set("date", dateFormat.formatEpochDate(d));
        updateDetails.set("documentId", documentId);
        return updateDetails;
    } else {
        log.log(`nothing found in result set for accountId ${accountId} and item Id ${itemId}`);
    }

    return undefined;
}

/**
 * The date needs to be in the format "2021-08-16T20:49:45Z"
 * @param details map with update details
 * @param accountId for logging
 * @param itemId for logging
 * @param documentId elasticsearch document id for the document to be updated
 * @param env the environment, can be either qa or prod
 * @param tryRun if true, then we skip the update call
 */
function updateFirstEnforcementDate (details, accountId, itemId, documentId, env, tryRun) {
    // call the update function now!
    if (details) {
        const date = details.get("date");
        log.info(`Will update main grid index (document id: "${documentId}") with date "${date}" for accountId ${accountId} and item id "${itemId}". Enforcement History Document Id: ${details.get("documentId")}`);
        if (tryRun) {
            log.warn("This is a try-run! Document will not be updated!");
        } else {
            postUpdate(documentId, date, env);
        }
    } else {
        console.warn(`Sorry, no date provided for Website document! Got: "${details}". Cannot proceed with update for accountId ${accountId}" and item id ${itemId}!`);
    }
}

/**
 *
 * @param result
 * @param env
 * @param tryRun
 * @returns {undefined}
 */
function triggerFixes(result, env, tryRun) {
    log.info("Result Total Hits WS:", result.hits.total);
    log.debug(JSON.stringify(result.hits, undefined, 2));
    if (!result.hits.total) {
        log.info("No matching record found!");
    }
    const hits = result.hits.hits;

    hits.forEach((h) => {
        const accountId = h._source.accountID;
        const globalItemId = h._source.globalItemId;
        const documentId = h._id;
        const firstEnforcementDate = h._source.enfFirstEnforcedDate;
        log.debug(`Calling fixer for AccountId: ${accountId} - GIID: ${globalItemId}. URLs document id: ${documentId}. Found as enfFirstEnforcedDate: ${firstEnforcementDate}`);
        retrieveEnforcementDateAndUpdateUrlsIndex(accountId, globalItemId, documentId, env, tryRun);
    });
    return undefined;
}

/**
 * Applies the update to the URLs Index for the first enforcement date only.
 *
 * @param documentId id of the document to be updated
 * @param enfFirstEnforceDate the date to update with
 * @param env the environment, either qa or prod
 */
function postUpdate (documentId, enfFirstEnforceDate, env) {
    fetch(`${urls.server.get(env)}/${urls.indexName.get(env)}/${urls.type}/${documentId}/_update`, urls.buildUpdateOptions(enfFirstEnforceDate))
        .then(response => response.json())
        .then(result => log.info(JSON.stringify(result, undefined, 2)))
        .catch(error => log.error("error", error));
}

/**
 * Orchestrates the update by finding the corresponding document in the history, then extracting the date and then
 * updating the original URLs document.
 * @param accountId
 * @param itemId
 * @param documentId
 * @param env the environment, either qa or prod
 * @param tryRun  if set to true does not run the update process
 */
function retrieveEnforcementDateAndUpdateUrlsIndex (accountId, itemId, documentId, env, tryRun) {
    fetch(`${enforcementHistory.server.get(env)}/${enforcementHistory.indexName.get(env)}/_search`, enforcementHistory.buildRequestOptions(accountId, itemId))
        .then(response => response.json())
        .then(result => extractDateFrom(result, accountId, itemId))
        .then(details => updateFirstEnforcementDate(details, accountId, itemId, documentId, env, tryRun))
        .catch(error => log.error("error", error));
}

/**
 * Main Entry Point. Locate all documents that need to be fixed and trigger the update process.
 * The date range defines the last enforcement date range of the URLs index.
 * @param size how many records do we want to work on with one call
 * @param from do we want to skip documents, then use from with greater than 0
 * @param startDate date range for matching records
 * @param endDate date range for matching records
 * @param env the environment, either qa or prod
 * @param tryRun if set to true does not run the update process
 */
function findDocumentsToRepairAndFixThem (size = 1, from = 0, startDate = "2021-08-01", endDate = "2021-09-01", env, tryRun) {
    log.info(`========== Start Locating Documents with Missing First Enforcement Date in Environment ${env}; Try-run? ${tryRun} ================`);
    fetch(`${urls.server.get(env)}/${urls.indexName.get(env)}/_search`, urls.buildRequestOptions(size, from, startDate, endDate))
        .then(response => response.json())
        .then(result => triggerFixes(result, env, tryRun))
        .catch(error => log.error("error", error));
}
const TRY_RUN = true;
const ENVIRONMENT = "prod"; // qa prod stage
findDocumentsToRepairAndFixThem(50, 0, "2021-07-01", "2021-10-01", ENVIRONMENT, TRY_RUN);
